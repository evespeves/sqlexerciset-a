-- SQL Script to build DB for simple PHP app
CREATE DATABASE deliveryInformation;

use deliveryInformation;

CREATE TABLE customer (
	customerId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	customerName VARCHAR(30) NOT NULL,
	customerEmail VARCHAR(50) NOT NULL,
	customerAddress VARCHAR(50) NOT NULL,
	customerPhoneNo INT(11) NOT NULL
);

-- Populate table
INSERT INTO customer VALUES(1,"Pearse Shilling","bob@hotmail.com", "33 Garratt Terrance, Tooting Broadway, London","07462280147");
INSERT INTO customer VALUES(2,"Mark Chinwag","chinwag@gmail.com","24 Blackforest Lane, Ealing, London","07843546453");
INSERT INTO customer VALUES(3,"SanMiguel Filmer","filmer@yahoo.com","2 Birchwood Rd.,Lincon Park, Kensington, London","0745245529");
INSERT INTO customer VALUES(4,"Tanya Sanatani","TanyaSanatani@gmail.com","Flat 1706, Heritage Tower, Crossharbour, London","07641698532");
INSERT INTO customer VALUES(5,"Eva Aherne","eva.aherne2@gmail.com","25 Raspberry Court, Stratford, London","07841692034");
INSERT INTO customer VALUES(6,"Mary Dwyer","mdwyer@hotmail.com", "22 Maggot Terrance, Tooting, London","07845136050");
INSERT INTO customer VALUES(7,"Eileen Aherne","eileenw365@gmail.com","24 BlackTree Lane, Elephant and Castle, London","078435487515");
INSERT INTO customer VALUES(8,"Tom Filmer","jellytots@yahoo.com","flat 56, 180 High Street, Stratford, London","07459368921");
INSERT INTO customer VALUES(9,"Frank Gallagher","frankyboy29@gmail.com","Flat 68, Liberty Tower, Blackwall, London","07678128262");
INSERT INTO customer VALUES(10,"Simon Pegg","spegg@gmail.com","217 Pegg Lane, Heathrow, London","07848745210");


CREATE TABLE suppliers (
	supplierId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	supplierName VARCHAR(30) NOT NULL,
	supplierEmail VARCHAR(50) NOT NULL,
	supplierAddress VARCHAR(50) NOT NULL,
	supplierPhoneNo INT(11) NOT NULL
);

INSERT INTO suppliers VALUES(1,"Deliveroo","deliveroo@hotmail.com", "Broadway, London","0214886120");
INSERT INTO suppliers VALUES(2,"Just Eat","just-eat@justeat.com","Ealing, London","02818956233");
INSERT INTO suppliers VALUES(3,"Uber Eats","uber-eats@yahoo.com","Kensington, London","027946656");
INSERT INTO suppliers VALUES(4,"GrubHub","grubhub@ghub.com","Crossharbour, London","02478932649");
INSERT INTO suppliers VALUES(5,"Meituan Waimai","meituan_waimai@mw.com","Stratford, London","0245692637");
INSERT INTO suppliers VALUES(6,"PizzaHut","pizzahut@pizza.com", "Tooting, London","07894962987");
INSERT INTO suppliers VALUES(7,"SnapFinger","SnapFinger@yahoo.com","Elephant and Castle, London","092878621");
INSERT INTO suppliers VALUES(8,"PostMates","postmates@mates.com","Stratford, London","0269721357");
INSERT INTO suppliers VALUES(9,"ShakeShack","ss@ShakeShack.com","Blackwall, London","0179656562");
INSERT INTO suppliers VALUES(10,"Carluccios","carolos@Carluccios.com","Heathrow, London","01979865332");

CREATE TABLE paymentDetails (
	paymentId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	paymentType VARCHAR(20) NOT NULL,
	cardNo INT(16) NOT NULL,
	expDate DATE timestamp,
	paid BOOLEAN DEFAULT FALSE NOT NULL
);

INSERT INTO paymentDetails VALUES(1,"Debit Card",1234567812345678, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(2,"Credit Card",2222333344445555, "2019-12-10 20:56:11", TRUE);
INSERT INTO paymentDetails VALUES(3,"Credit Card",2222333344445555, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(4,"Debit Card",9876987698769876, "2030-06-12 12:10:10", FALSE);
INSERT INTO paymentDetails VALUES(5,"Credit Card",3333444455556666, "2035-07-02 13:10:10", FALSE);

INSERT INTO paymentDetails VALUES(6,"Debit Card",1234567812345678, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(7,"Credit Card",2222333344445555, "2019-12-10 20:56:11", TRUE);
INSERT INTO paymentDetails VALUES(8,"Credit Card",2222333344445555, "2022-06-12 23:05:10", TRUE);
INSERT INTO paymentDetails VALUES(9,"Debit Card",9876987698769876, "2030-06-12 12:10:10", FALSE);
INSERT INTO paymentDetails VALUES(10,"Credit Card",3333444455556666, "2035-07-02 13:10:10", FALSE);

CREATE TABLE orderDetails (
	orderId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	customerId INT NOT NULL,
	paymentId INT NOT NULL,
	driverId INT NOT NULL,ß
	supplierId INT NOT NULL,
	itemsOrdered VARCHAR(265),
	paymentId INT(11),
	
	FOREIGN KEY (customerId) 
        REFERENCES customer(customerId),
        
	FOREIGN KEY (paymentId) 
        REFERENCES paymentDetails(paymentId),
    
    FOREIGN KEY (supplierId) 
        REFERENCES suppliers(supplierId),
        
    FOREIGN KEY (driverId) 
        REFERENCES driver(driverId)
);

CREATE TABLE driver (
	driverId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	driverName VARCHAR(30) NOT NULL,
	driverEmail VARCHAR(30) NOT NULL,
	vehicleType VARCHAR(30) NOT NULL,
	driverPhone INT(11) NOT NULL
);

INSERT INTO driver VALUES(1,"Matt Davidson","mattyDav@hotmail.com", "Car","07989232926");
INSERT INTO driver VALUES(2,"Dave Harleyson","davyHarl@justeat.com","Ealing, London","02818956233");
INSERT INTO driver VALUES(3,"Frank Sinatra","frankySinatra@yahoo.com","Kensington, London","027946656");
INSERT INTO driver VALUES(4,"Rudhi Garg","garg.rudhi@ghub.com","Crossharbour, London","02478932649");
INSERT INTO driver VALUES(5,"Gorge Gillings","gillingsGorge@mw.com","Stratford, London","0245692637");
INSERT INTO driver VALUES(6,"Paur Yambolv","PaurYambolv@pizza.com", "Tooting, London","07894962987");
INSERT INTO driver VALUES(7,"Yanseul Yo","yoHans@yahoo.com","Elephant and Castle, London","092878621");
INSERT INTO driver VALUES(8,"Alisinder Tityat","alis_tit@mates.com","Stratford, London","0269721357");
INSERT INTO driver VALUES(9,"Stephaine Jelehan","sj@ShakeShack.com","Blackwall, London","0179656562");
INSERT INTO driver VALUES(10,"Nyasha Mutangadura","nyasha_m@gmail.com","Heathrow, London","01979865332");
